data:extend({
    {
    type = "technology",
    name = "alien-tech",
    icon = "__base__/graphics/technology/advanced-chemistry.png",
    prerequisites = {"military-4", "speed-module-3", "productivity-module-3"},
    unit =
    {
      count = 200,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1},
        {"alien-science-pack", 1}
      },
      time = 60
    },
    order = "i-c-c"
  },
  {
    type = "technology",
    name = "alien-cloning",
    icon = "__base__/graphics/technology/advanced-chemistry.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "alien-clone"
      }
    },
    prerequisites = {"alien-tech"},
    unit =
    {
      count = 400,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1},
        {"alien-science-pack", 1}
      },
      time = 60
    },
    order = "i-c-d"
  },
  {
    type = "technology",
    name = "alien-weapons",
    icon = "__base__/graphics/technology/advanced-chemistry.png",
    prerequisites = {"alien-tech"},
    unit =
    {
      count = 300,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1},
        {"alien-science-pack", 1}
      },
      time = 60
    },
    order = "i-c-e"
  },
  {
    type = "technology",
    name = "laser-turret-damage-7",
    icon = "__base__/graphics/technology/laser-turret-damage.png",
    effects =
    {
      {
        type = "ammo-damage",
        ammo_category = "laser-turret",
        modifier = "0.5"
      }
    },
    prerequisites = {"laser-turret-damage-6", "alien-weapons"},
    unit =
    {
      count = 450,
      ingredients =
      {
        {"alien-science-pack", 1},
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 70
    },
    upgrade = true,
    order = "e-n-g"
  },
  {
    type = "technology",
    name = "laser-turret-damage-8",
    icon = "__base__/graphics/technology/laser-turret-damage.png",
    effects =
    {
      {
        type = "ammo-damage",
        ammo_category = "laser-turret",
        modifier = "0.6"
      }
    },
    prerequisites = {"laser-turret-damage-7"},
    unit =
    {
      count = 600,
      ingredients =
      {
        {"alien-science-pack", 1},
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 85
    },
    upgrade = true,
    order = "e-n-h"
  },
  {
    type = "technology",
    name = "laser-turret-damage-9",
    icon = "__base__/graphics/technology/laser-turret-damage.png",
    effects =
    {
      {
        type = "ammo-damage",
        ammo_category = "laser-turret",
        modifier = "0.7"
      }
    },
    prerequisites = {"laser-turret-damage-8"},
    unit =
    {
      count = 800,
      ingredients =
      {
        {"alien-science-pack", 1},
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 100
    },
    upgrade = true,
    order = "e-n-i"
  },
  {
    type = "technology",
    name = "laser-turret-damage-10",
    icon = "__base__/graphics/technology/laser-turret-damage.png",
    effects =
    {
      {
        type = "ammo-damage",
        ammo_category = "laser-turret",
        modifier = "0.8"
      }
    },
    prerequisites = {"laser-turret-damage-9"},
    unit =
    {
      count = 1000,
      ingredients =
      {
        {"alien-science-pack", 1},
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 120
    },
    upgrade = true,
    order = "e-n-j"
  },
  {
    type = "technology",
    name = "gun-turret-damage-7",
    icon = "__base__/graphics/technology/gun-turret-damage.png",
    effects =
    {
      {
        type = "turret-attack",
        turret_id = "gun-turret",
        modifier = "0.5"
      }
    },
    prerequisites = {"gun-turret-damage-6", "alien-weapons"},
    unit =
    {
      count = 350,
      ingredients =
      {
        {"alien-science-pack", 1},
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 65
    },
    upgrade = true,
    order = "e-o-g"
  },
  {
    type = "technology",
    name = "gun-turret-damage-8",
    icon = "__base__/graphics/technology/gun-turret-damage.png",
    effects =
    {
      {
        type = "turret-attack",
        turret_id = "gun-turret",
        modifier = "0.6"
      }
    },
    prerequisites = {"gun-turret-damage-7"},
    unit =
    {
      count = 400,
      ingredients =
      {
        {"alien-science-pack", 1},
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 70
    },
    upgrade = true,
    order = "e-o-h"
  }
})
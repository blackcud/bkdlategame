  data:extend({
  {
    type = "recipe",
    name = "alien-clone",
    energy_required = 45,
    category = "advanced-crafting",
    ingredients =
    {
      {"productivity-module", 4},
      {"assembling-machine-3", 1},
      {"low-density-structure", 4},
      {"solid-fuel",2},
      {"alien-artifact", 19},
      {type="fluid", name="water", amount=8}
    },
    result = "alien-artifact",
    result_count = 20,
    enabled = false
  }
})